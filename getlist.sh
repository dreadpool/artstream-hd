#!/bin/bash

if [ ! -d "/storage" ]; then
  mkdir /storage
  if [ ! -d "/storage" ]; then
  	echo "Error! Try running as root."
  	exit
  fi
fi

mkdir /storage/tmp
  if [ ! -d "/storage/tmp" ]; then
  	echo "Error! Try running as root."
  	exit
  fi

echo "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA2WxfPgmfklMfCYNIPf3MA1nCW5bUxYz6jGLByBGOGckjicdH
85Sv67wUjfUDSNw0DPOVWrp3z2XJ6hlHPWec6ZjmlZPDEGpAaToxxBclGzE6vTZd
5g3URuLXBX5zkObrsqDMDz3kRnXtfKSYVnac3s20gC7CW5W+Qyhtxcze+IuLf53/
jGEUU51p8WVpO65N4fSn5Os+ib/lRQdkrfzYhDceLOo+4RzkG0dz+Bdf6X6aaymC
X7fs8CFjQXOZkqTCmsbqOe/2rz3+JiFkrDoHLL3kI/KXlFAzE5M3vJmmKEHjNP11
sM3ZgA/TdmTt2x5rcq9XlRAY833B84pOdN98OQIDAQABAoIBAQDMl0qkEOAkC61o
AS4ojzq7aGoYTz0WS8uz8HYSclp57yhOzMPq9F36ADiDQCwAEp20Fj3TayW0dj7i
1ziPEoG32pYX6DwkpSbJHeaYc4MKLG/ufksrYJYAwhG9bLkIgBMIOoDqUr4Mg4me
wS9MJT8F9SFFgTqIfcbUYSWyfE7QJptmOVxbdQN+r0BTggRT2pLgUTOmCYrLyP5Y
8pchyQ/Yzq+Qzlpm0XzzkYPLDLlZ4baprtuGU0fgvfkFAFQSV/5o0X58e8WLcfjM
uOCTC3Iua9fFBiKAgndCRTanrBiKB1KaU55hV9/HXQKYTw5vsL+ZDMxT8mJjEVCw
+nKSDzbRAoGBAO98FljaCpBa6wxJ9z9olJ/kLqqcRWTgoOoRO+8WcBGD3f6DLv72
kXarOC8t8P91o+/WzmFx0+7cUTVjaxytlJEJwDLlfrbIb0PuXPlI9lr8epK9/cpE
R/wlKITCKZ3fLApUabjL0T3yC42f9wRzxtn38u8k5YOPzksBnc/3aJ3lAoGBAOhq
zvZM+gkQbQ/FM4Y34Rd5PfaUH8NhO0RWm2Av1v5x/CCIYH7SmuuzQdqJmMDB+8hE
Z+0Hv8sRQsYcyyqHNFju3Ugj5KHHDYrSHkmWzBR1cliWtEHODu6inTC/Ws5yRikJ
zCugs+QCOvG3a97SxjJH62f9qkbtAQYuRfZnTl/FAoGAejMmMAZqd4h9AZNIf2l8
1S2ZBmOhIT1hvq42l+QqITXZdAI6W80zIotxqtlVbPT5Ruuj+aMckniHU6e8QzDr
ayq/kPj7tIcIB3C8rlMlqf3FCdW6h5hZ5vaK+1sb4zu6fDbgj3Qo++PjM0wZvoLZ
R+W2Z4K7PmuFx0422aSUFGUCgYEAgfd4VHMnPvpYgoEcFuoYjS/XXpPmTzOIGOb1
bAxUhWToQKkCjOjoiPVs1fAr416WlZ7rHrk+nye/11kyo02IEFNCmpy3tV/CSubE
2RHifW5Pywo7MrEv2HJGxIO9G8T7RuebrKno7o/wtkqDn87t1iAxm61WEuNltzcZ
GHkccqECgYEAkLvdOT2G4cQEE/X0n5jY2q8+w9QlkGJ6y+S2yknqglBMCgzwJpaI
t/hPkme/aAvouBtSn/kbH4K9bmb01oNv1B37eJfGV/+vOXB6F+PH+AdTjR6ZwRXl
rLd8xw7EQ4oAx4wHUvDKCRp1sBzofg1Wzlhf3ColKTw/76QHl3EunKI=
-----END RSA PRIVATE KEY-----" >> /storage/tmp/id_rsa

ident="/storage/tmp/id_rsa"

chmod 600 $ident

if [ ! -d "/storage/pictures" ]; then
  mkdir /storage/pictures
fi

printf "Retrieving category list...."
sftp -pq -i $ident default@fallingrayne.dyndns-wiki.com >> /storage/tmp/getlist.log <<!
  get -p list /storage/tmp
  bye
!
echo "done!"
echo -e "\nSelect the number for the option you would like.\n"
while read line
do
echo $line
done < "/storage/tmp/list"

printf "\nChoice: "
read num

# if [ -d "/storage/pictures/cache" ]; then
#   rm -r /storage/pictures/cache
# fi

if [ -n "$(ls -A /storage/pictures)" ]; then
  rm /storage/pictures/*
fi

line=$(sed -n ${num}p /storage/tmp/list)

sftp -pq -i $ident default@fallingrayne.dyndns-wiki.com >> /storage/tmp/getlist.log <<!
  get -pr images/"${line}"/* /storage/pictures
  bye
!

# rm /tmp/getlist.log
# rm /storage/pictures/list

 rm -r /storage/tmp
